<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inicia sesión</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.4/css/bulma.min.css">
    <style>
        body {
            background-color: #f2f2f2;
            height: 100vh;
        }
        .section {
            width: 650px;
        }
    </style>

<body>
    <div class="container is-flex is-justify-content-center is-align-content-center">
        <section
            class="login section is-medium  is-flex is-justify-content-center is-align-content-center is-flex-direction-column">
            <h3 class="title is-3 has-text-centered">Inicia Sesión</h3>
            <div class="block">
                <form>
                    <div class="field">
                        <label class="label">Usuario</label>
                        <div class="control">
                            <input class="input is-medium" autocomplete="off" type="text" placeholder="Text input" autofocus>
                        </div>
                    </div>

                    <div class="field">
                        <label class="label">Contraseña</label>
                        <div class="control">
                            <input class="input is-medium" autocomplete="off" type="password" placeholder="Text input">
                        </div>
                    </div>

                    <div class="control mt-5">
                        <button class="button is-success is-medium">Inicia sesión</button>
                    </div>
                </form>
            </div>
        </section>
    </div>
</body>

</html>