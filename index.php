<?php
// zona horaria america central

date_default_timezone_set('US/Central');

/**
 * inicio de sesiones
 * session_start();
 */

$app = "app/";
$path_controller = "controller/";

if (!isset($_REQUEST['controller'])):
    // vista por defecto
    require_once $app . $path_controller . "index_controller.php";
    $controller = new IndexController();
    $controller->index();
else:
    $controller = $_REQUEST['controller'];
    $action = $_REQUEST['action'];
    $file = $app . $path_controller . $controller . "_controller.php";
    if (is_file($file)) {
        // verifica si el archivo controlador existe
        require_once $file;

        try {
            // comprueba que la accion este registrada
            $controller = ucwords($controller) . 'Controller';
            $controller = new $controller;
            call_user_func([$controller, $action]);
        } catch (\Throwable $th) {
            require_once  $app . $path_controller . "error_controller.php";
            $controller = new ErrorController();
            $controller->index();
        }
    } else {
        require_once  $app . $path_controller . "error_controller.php";
        $controller = new ErrorController();
        $controller->index();
    }
endif;